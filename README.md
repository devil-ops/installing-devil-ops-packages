# Installing Packages from Devil Ops

We package up a number of our devil-ops projects in to executables, linux
packages and more! If you would like help packaging your app in an automated
way, feel free to reach out here (or just ping @drews). 

While you can always download the packages directly from the `releases` tab on
the project, you may also use certain third party tools to get more automated
updates. These tools are described below

## macOS

### Homebrew

Homebrew formulas can be 'tapped', using:

```shell
$ brew tap devil-ops/devil-ops https://gitlab.oit.duke.edu/devil-ops/homebrew-devil-ops.git
...
```

## Linux

We have a expiramental deb/yum repos that live on https://oneget.oit.duke.edu.
These should work if you want to use them for updates, however we have not yet
done a ton of testing on this. Please give them a shot though and let us know if
you find any issues.

### Debian/Ubuntu distributions

#### 20.04 and earlier

```shell
wget -qO - https://oneget.oit.duke.edu/debian-feeds/devil-ops-debs.pub | sudo apt-key add -
echo "deb https://oneget.oit.duke.edu/ devil-ops-debs main" | sudo tee /etc/apt/sources.list.d/devil-ops.list
```

#### 22.04 and later

Ubuntu 22.04 and later have deprecated `apt-key` for adding apt keys. Instead,
we use the binary gpg key, per recommendations from the `apt-key` man page.

```
wget -qO - https://oneget.oit.duke.edu/debian-feeds/devil-ops-debs.pub | sudo gpg --dearmor -o /etc/apt/keyrings/devil-ops-debs.gpg
echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/devil-ops-debs.gpg] https://oneget.oit.duke.edu/ devil-ops-debs main" | sudo tee /etc/apt/sources.list.d/devil-ops.list
```



### CentOS/RHEL distributions

The RPM hosting software does not yet support gpg signing of packages, but it
is on the roadmap. If you would like to verify your package, you can use the
SHA256SUMS file provided on the release page for a given project.

We have also started to sign the packages outside of the hosting software.
Keeping in mind that we have not yet signed all of the packages in the
repository, the ones we have signed can be validated with [this
key](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages/-/raw/main/pubkeys/RPM-GPG-KEY-DEVIL-OPS-2022-05-02).

Create the file `/etc/yum.repos.d/devil-ops.repo` with the following contents:

```ini
[devil-ops]
name=devil-ops
baseurl=https://oneget.oit.duke.edu/rpm/devil-ops-rpms/
gpgcheck=0
enabled=1
```

or for required gpg checks (Again, keep in mind not all packages are signed yet)

```ini
[devil-ops]
name=devil-ops
baseurl=https://oneget.oit.duke.edu/rpm/devil-ops-rpms/
gpgkey=https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages/-/raw/main/pubkeys/RPM-GPG-KEY-DEVIL-OPS-2022-05-02
gpgcheck=1
enabled=1
```

## Windows

### Scoop

[Scoop](https://scoop.sh/) packages can be installed from custom 'buckets'. 

```shell
scoop bucket add devil-ops https://gitlab.oit.duke.edu/devil-ops/devil-ops-scoop.git
scoop install devil-ops/my-devil-ops-package
```
